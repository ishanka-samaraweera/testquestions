@extends('app')

@section('content')

<div class="content">
  <div class="row">
    <div class="col-md-4 center">
    <form action="/" method="post">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="telephone_number">Phone Number</label>
        <input type="text" class="form-control" name="telephone_number" value="{{ old('telephone_number', @$number) }}">
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    @if ($errors->has('telephone_number'))
      <span class="text-danger">
          <strong>{{ $errors->first('telephone_number') }}</strong>
      </span>
    @endif
    @if(isset($country ))
        <p>This number related to {{ $country }}</p>
    @endif
  </div>
  <div class="col-md-4 center">
    <form action="/show-number" method="post">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="mnemonic_number">Phone number with mnemonic word</label>
        <input type="text" class="form-control" name="mnemonic_number" value="{{ old('mnemonic_number', @session('mnemonic_number')) }}">
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    @if ($errors->has('mnemonic_number'))
      <span class="text-danger">
          <strong>{{ $errors->first('mnemonic_number') }}</strong>
      </span>
    @endif
    @if(session('new_number'))
      <p>This number is {{ session('new_number') }}</p>
    @endif

  </div>

  </div>
</div>

@endsection
