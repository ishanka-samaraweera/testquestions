This project has been done using Laravel framework.

You need to have php 7.0 or greater run this project.

 

How to setup.
--------------

·         Clone the project to your PC.

·         Through the terminal, go inside the project folder and run following commands.

				composer update

				php artisan serve

·         After run this command you can check the url it returns. Most of the time localhost:8000

 

Note.
-------

I have used only following countries in my list to test.

   +1 - Canada

   +2 - Africa

   +20 - Egypt

   +211 - South Sudan

   +212 - Morocco

   +213 - Algeria

   +216 - Tunisia

   +218 – Libya

   +220 - Gambia

   +221 - Senegal

   +222 - Mauritania

   +223 - Mali

   +224 - Guinea

   +225 - Ivory Coast

   +226 - urkina Faso

   +30 - Greece

   +31 - Netherlands

   +32 - Belgium

   +33 - France

   +34 - Spain

   +350 - Gibraltar

   +351 - Portugal

   +352 - Luxembourg

   +353 - Ireland

   +354 - Iceland

   +355 - Albania

   +356 - Malta

   +357 - Cyprus

   +358 - Finland

   +359 – Bulgaria

 

When enter anther code it gives invalid country message. The data set is on config/countries.php file.

For telephone numbers, starting + sign, - and spaces are allowed. Test cases are written only for telephone number validation.

