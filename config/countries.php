<?php

return [

  '1' => [
    '-1' => 'Canada'
  ],
  '2' => [
    '-1' => 'Africa',
    '0' =>
      [
        '-1' => 'Egypt'
      ],
    '1' =>
      [
        '1' => 'South Sudan',
        '2' => 'Morocco',
        '3' => 'Algeria',
        '6' => 'Tunisia',
        '8' => 'Libya'
      ],
    '2' =>
      [
        '0' => 'Gambia',
        '1' => 'Senegal',
        '2' => 'Mauritania',
        '3' => 'Mali',
        '4' => 'Guinea',
        '5' => 'Ivory Coast',
        '6' => 'Burkina Faso'
      ]
  ],
  '3' =>
  [
    '0' =>
    [
      '-1' => 'Greece'
    ],
    '1' =>
    [
      '-1' => 'Netherlands'
    ],
    '2' =>
    [
      '-1' => 'Belgium'
    ],
    '3' =>
    [
      '-1' => 'France'
    ],
    '4' =>
    [
      '-1' => 'Spain'
    ],
    '5' =>
    [
      '0' => 'Gibraltar',
      '1' => 'Portugal',
      '2' => 'Luxembourg',
      '3' => 'Ireland',
      '4' => 'Iceland',
      '5' => 'Albania',
      '6' => 'Malta',
      '7' => 'Cyprus',
      '8' => 'Finland',
      '9' => 'Bulgaria'
    ]
  ]

];
