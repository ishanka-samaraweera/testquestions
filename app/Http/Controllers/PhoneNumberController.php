<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PhoneNumberController extends Controller
{

    /**
    * Display the country belongs to telephone number
    * @para Request $request
    *
    * return view
    **/
    public function showCountry(Request $request){
      $telephone_number = preg_replace('/[\s-]+/', '', $request->telephone_number);
      $request->merge(array('telephone_number' => $telephone_number));
      $this->isValidPhoneNumber($request->all())->validate();

      $telephone_number = preg_replace('/[\^+]/', '', $request->telephone_number);
      $country_code_first_digit = substr($telephone_number, 0, 1);
      $country_code_second_digit = substr($telephone_number, 1, 1);
      $country_code_third_digit = substr($telephone_number, 2, 1);

      $countries = Config('countries');
      if(isset($countries[$country_code_first_digit][$country_code_second_digit][$country_code_third_digit])){
        $country = $countries[$country_code_first_digit][$country_code_second_digit][$country_code_third_digit];
      }
      else if(isset($countries[$country_code_first_digit][$country_code_second_digit][-1])){
        $country = $countries[$country_code_first_digit][$country_code_second_digit][-1];
      }
      else if(isset($countries[$country_code_first_digit][-1])){
        $country = $countries[$country_code_first_digit][-1];
      }
      else{
        $country = "Invalid country";
      }

      return view('phone')->with('country', $country)->with('number', $request->telephone_number);

    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function isValidPhoneNumber($data){

      return Validator::make($data, [
          'telephone_number' => 'required|regex:/^\+?[1-9]\d{7,14}$/'
      ]);

    }

    /**
    * Display the phone number by mnemonics word
    * @para Request $request
    *
    * return view
    **/
    public function showNumber(Request $request){
      $mnemonic_number = preg_replace('/[\s-]+/', '', $request->mnemonic_number);
      $request->merge(array('mnemonic_number' => $mnemonic_number));
      $this->isValidMnemonicPhoneNumber($request->all())->validate();

      $number = Config('numbers');

      $telephone_number = preg_replace('/[\^+]/', '', $request->mnemonic_number);
      $letters = str_split(strtoupper($telephone_number));
      $new_number = "";
      foreach($letters as $letter){
        if(!is_numeric($letter)){
          $new_number .= $number[$letter];
        }
        else{
          $new_number .= $letter;
        }
      }

      return redirect('/')->with('new_number', $new_number)->with('mnemonic_number', $request->mnemonic_number);

    }

    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function isValidMnemonicPhoneNumber($data){

      return Validator::make($data, [
          'mnemonic_number' => 'required|regex:/^\+?[A-Za-z0-9]{8,15}$/'
      ]);

    }

}
