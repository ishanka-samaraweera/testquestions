<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhoneNumberTest extends TestCase
{
    /**
     * Test max length validation of Telephone number.
     *
     * @return void
     */
    function test_telephone_number_max_length()
    {
           $response = $this->post('/', [
               'telephone_number' => '123-2345678334567'
           ]);

           $response->assertStatus(302);
           $response->assertSessionHasErrors([
               'telephone_number' => 'The telephone number format is invalid.'
           ]);
    }

    /**
     * Test invalid characters of Telephone number.
     *
     * @return void
     */
    function test_telephone_number_invalid_character()
    {
           $response = $this->post('/', [
               'telephone_number' => '123-234$567'
           ]);

           $response->assertStatus(302);
           $response->assertSessionHasErrors([
               'telephone_number' => 'The telephone number format is invalid.'
           ]);
    }

    /**
     * Test telephone number with letters.
     *
     * @return void
     */
    function test_telephone_number_with_letter()
    {
           $response = $this->post('/', [
               'telephone_number' => '123ABC234567'
           ]);

           $response->assertStatus(302);
           $response->assertSessionHasErrors([
               'telephone_number' => 'The telephone number format is invalid.'
           ]);
    }

    /**
     * Test telephone number less than min length.
     *
     * @return void
     */
    function test_telephone_number_less_than_min()
    {
           $response = $this->post('/', [
               'telephone_number' => '1234567'
           ]);

           $response->assertStatus(302);
           $response->assertSessionHasErrors([
               'telephone_number' => 'The telephone number format is invalid.'
           ]);
    }

    /**
     * Test valid Telephone number.
     *
     * @return void
     */
    function test_telephone_number_with_spaces()
    {
           $response = $this->post('/', [
               'telephone_number' => '123 234 5673454'
           ]);

           $response->assertStatus(200);
    }

    /**
     * Test valid Telephone number.
     *
     * @return void
     */
    function test_telephone_number_with_hypens()
    {
           $response = $this->post('/', [
               'telephone_number' => '123-234-5673454'
           ]);

           $response->assertStatus(200);
    }

    /**
     * Test valid Telephone number.
     *
     * @return void
     */
    function test_telephone_number_start_with_plus_sign()
    {
           $response = $this->post('/', [
               'telephone_number' => '+123-234-5673454'
           ]);

           $response->assertStatus(200);
    }

    /**
     * Test valid Telephone number.
     *
     * @return void
     */
    function test_telephone_number_with_eight_numbers()
    {
           $response = $this->post('/', [
               'telephone_number' => '123-23445'
           ]);

           $response->assertStatus(200);
    }
}
